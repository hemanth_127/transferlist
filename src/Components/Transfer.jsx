import React, { useState } from 'react'

const Transfer = () => {


  const rawData = [
    { id: 1, name: 'Java', isleft: true },
    { id: 2, name: 'python', isleft: true },
    { id: 3, name: 'php', isleft: false },
    { id: 4, name: 'jquery', isleft: true },
    { id: 5, name: 'c++', isleft: false },
    { id: 6, name: 'c', isleft: true },
    { id: 7, name: 'javaScript', isleft: false },
    { id: 8, name: 'React', isleft: true }
  ]


  const [data, setData] = useState(rawData)
  const [check, setCheck] = useState([])
  const middle = ['>>', '>', '<', '<<']

  const filterData = data.reduce(
      (acc, ele) => {
        if (ele.isleft) {
            if (!acc.leftpos) {
              acc[leftpos] = []
            }
            acc.leftpos.push(ele)
        } else {
            if (!acc.rightpos) {
              acc.rightpos = []
            }
            acc.rightpos.push(ele)
        }
        return acc
      },
      { leftpos: [], rightpos: [] }
  )



  const allLeft = () => {
      setData(d=>
          d.map(ele => {
            return { ...ele, isleft: false }
          })
      )
  }



  const allRight = () => {
      setData(d=>
          d.map(ele => {
            return { ...ele, isleft: true }
          })
      )
  }



  const handleClick = e => {
      if (e.target.value === '>>') {
        allLeft()
      } else if (e.target.value === '<<') {
        allRight()
      } else if (e.target.value === '>') {
        toggleCheck(false)
      } else {
        toggleCheck(true)
      }
  }



  const toggleCheck = isleft => {
      setData(d=>
        d.map(ele => {
            if (check.includes(ele.name)) {
              return { ...ele, isleft: isleft }
            }
            return ele
        })
      )
      setCheck([])
  }



  const handleCheck = e => {
      const { name, checked } = e.target
      if (checked) {
        setCheck([...check, name])
      } else {
        setCheck(check.filter(ele => ele !== name))
      }
  }



    return (
      <div style={{ display: 'flex', justifyContent: 'space-evenly' }}>

          <div style={{ display: 'flex', flexDirection: 'column', padding: '1rem' }} >
            {filterData.leftpos.map(({ id, name}) => {
              return (
                <label key={id}>
                    <input
                      type='checkbox'
                      name={name}
                      checked={check.includes(name)}
                      onChange={handleCheck}
                    />
                  {name}
                </label>
              )
            })}
          </div>


          <div style={{ padding: '1rem', display: 'flex', flexDirection: 'column' }}   >
              {middle.map((item, index) => {
                return (
                  <button key={index} value={item} onClick={handleClick}>
                    {item}
                  </button>
                )
              })}
          </div>


          <div style={{ display: 'flex', flexDirection: 'column', padding: '1rem' }} >
            {filterData.rightpos.map(({ id, name}) => {
                return (
                  <label key={id}>
                    <input 
                      type='checkbox'
                      name={name} 
                      checked={check.includes(name)}
                      onChange={handleCheck} 
                    />
                    {name}
                  </label>
                )
            })}
          </div>
      </div>
    )
}

export default Transfer
